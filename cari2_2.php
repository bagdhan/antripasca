 
 <!DOCTYPE html>
<?php
// memulai session
session_start();
error_reporting(0);
if (isset($_SESSION['level']))
{
    // jika level admin
    if ($_SESSION['level'] == "admin")
   {   
   }
   // jika kondisi level user maka akan diarahkan ke halaman lain
   else if ($_SESSION['level'] == "1")
   {
       header('location:index.php');
   }
}
if (!isset($_SESSION['level']))
{
    header('location:login.php');
}
$username= $_SESSION['username'];
 ?>
<html>
  <head>
  <style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
   <title>Antrian</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="assets/demo.css">
  <link rel="stylesheet" href="assets/header-fixed.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">  
        <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link href="scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link rel="stylesheet" href="select2-master/dist/css/select2.min.css"/>
     <!--<script src="js/jquery1.js"></script>!-->
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <!-- Logo -->
                  <div class="logo">
                     <h1><a href="antrian.php">Antrian Sekolah Pascasarjana</a></h1>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                    <div class="col-lg-12">
                      
                    </div>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="navbar navbar-inverse" role="banner">
                      <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                             <a href="logout.php" >LogOut (<?php echo $_SESSION['username'];?>)</a>
                            
                          </li>
                        </ul>
                      </nav>
                  </div>
               </div>
            </div>
         </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                     <li><a href="kelola_user.php"> Kelola Data User</a> </li>
                        <li><a href="kelola_mahasiswa.php"> Kelola Data Mahasiswa</a> </li>
                        <li><a href="kelola_pengajuan.php"> Kelola Data Pengajuan</a></li>
                        <li><a href="cari2.php"> Antrian Sudah Diproses</a></li>
						<li><a href="cari2_2.php"> Antrian yang Sudah Distatuskan</a></li>
                        <!--<li><a href="histori.php">Antrian</a></li>
                        <li><a href="info.php">Informasi</a></li>-->
                    
                </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="row"></div>

            <div class="row">
                <div class="col-md-12 panel-warning">
                    <div class="content-box-header panel-heading">
                      <div class="panel-options">
                        </div>
                  </div>
                    <div class="content-box-large box-with-header">
                       
            
<?php 
 
$link = mysqli_connect("localhost", "root", "", "antrian");
 
?>  <tr>
<div class="panel-heading">
<div align="center" class="panel-title">Antrian yang Sudah Distatuskan</div>
<!--<br><br><a href="hapus_proses.php" class="btn btn-danger">Hapus Semua Data Proses</a>-->
<div class="panel-body">
<font  style="font-size:16px" color="red">Perhatian: Jika ingin merubah status proses pengajuan, silahkan klik tombol "Pensil" pada kolom "Kelola".</font>
<!--<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Aktif')">Mahasiswa Aktif</button>
  <button class="tablinks" onclick="openCity(event, 'Non_aktif')">Mahasiswa Non Aktif</button>
</div>
<div id="Aktif" class="tabcontent">
<table id="example1" class="table table-striped table-bordered">
<thead> 
<tr>
<td width="50"><b>No.</b></td>
<td width="50"><b>No. Registrasi</b></td>
<td width="50"><b>NRP</b></td>
<td width="100"><b>Nama Mahasiswa</b></td>
<td width="20"><b>Nama Surat</b></td>
<td width="20"><b>Estimasi Selesai</b></td>
<td width="20"><b>Tanggal Pengajuan</b></td>
<td width="20"><b>Status</b></td>
<td width="20"><b>Tanggal Proses</b></td>
<td width="20"><b>Petugas</b></td>
<td width="20"><b>Kelola</b></td>
<!--<td width="20"><b>Hapus</b></td>!-->
<!--</tr>
</thead>
			<tbody>
<?php
 $no=1;
$result = mysqli_query($link, "SELECT * FROM proses,mahasiswa_aktif,pengajuan,user where proses.nrp=mahasiswa_aktif.nrp and proses.id_pengajuan=pengajuan.id_pengajuan and proses.id_user=user.id_user ORDER BY id_proses");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{  
 
?> 
<tr>
<td><?php echo $no; ?></td>
<td><?php echo $buff['id_proses']; ?></td>
<td><?php echo $buff['nrp']; ?></td>
<td><?php echo $buff['nama']; ?></td>
<td><?php echo $buff['nama_surat']; ?></td>
<td><?php echo $buff['estimasi']; ?></td>
<td><?php echo $buff['tanggal_pengajuan']; ?></td>
<td><?php echo $buff['status']; ?></td>
<td><?php echo $buff['tanggal_diproses']; ?></td>
<td><?php echo $buff['nama_user']; ?></td>

<td align="center"><a href="update_proses2_aktif.php?id_proses=<?php echo $buff['id_proses']; ?>"><img src='edit.png' style='width:20px; height:17px;'></a><!--<a href="del5.php?nrp=<?php echo $buff['nrp']; ?>"> <img src='del.png' style='width:20px; height:17px;'> </a>--></td>
<!--</tr>

<?php
$no++; };

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
   </tbody>
		</table>
</div>
<div id="Non_aktif" class="tabcontent">-->
<table id="example" class="display" style="width:100%">
<thead> 
<tr>
<th><b>No.</b></th>
<th><b>No. Registrasi</b></th>
<th><b>NRP</b></th>
<th><b>Nama Mahasiswa</b></th>
<th><b>Nama Surat</b></th>
<th><b>Estimasi Selesai</b></th>
<th><b>Keterangan</b></th>
<th><b>Catatan</b></th>
<th><b>Tanggal Pengajuan</b></th>
<th><b>Status</b></th>
<th><b>Tanggal Diambil/Pending</b></th>
<th><b>Petugas</b></th>
<th><b>Kelola</b></th>
<!--<td width="20"><b>Hapus</b></td>!-->
</tr>
</thead>
			<tbody>
<?php
$link2 = mysqli_connect("localhost", "root", "", "antrian");
 $no=1;
$result = mysqli_query($link2, "SELECT * FROM proses,mahasiswa,pengajuan,user where proses.nrp=mahasiswa.nrp and proses.id_pengajuan=pengajuan.id_pengajuan and proses.id_user=user.id_user and status not like 'Proses' ORDER BY id_proses");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{  
 
?> 
<tr>
<td><?php echo $no; ?></td>
<td><?php echo $buff['id_proses']; ?></td>
<td><?php echo $buff['nrp']; ?></td>
<td><?php echo $buff['nama']; ?></td>
<td><?php echo $buff['nama_surat']; ?></td>
<td><?php echo $buff['estimasi']; ?></td>
<td><?php echo $buff['keterangan']; ?></td>
<td><?php echo $buff['catatan']; ?></td>
<td><?php echo $buff['tanggal_pengajuan']; ?></td>
<td><?php echo $buff['status']; ?></td>
<td><?php echo $buff['tanggal_diproses']; ?></td>
<td><?php echo $buff['nama_user']; ?></td>

<td align="center"><a href="update_proses2_tidakaktif.php?id_proses=<?php echo $buff['id_proses']; ?>"><img src='edit.png' style='width:20px; height:17px;'></a><!--<a href="del5.php?nrp=<?php echo $buff['nrp']; ?>"> <img src='del.png' style='width:20px; height:17px;'> </a>--></td>
</tr>

<?php
$no++; };

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
   </tbody>
   <tfoot>
            <tr>
                <th><b>No.</b></th>
<th><b>No. Registrasi</b></th>
<th><b>NRP</b></th>
<th><b>Nama Mahasiswa</b></th>
<th><b>Nama Surat</b></th>
<th><b>Estimasi Selesai</b></th>
<th><b>Keterangan</b></th>
<th><b>Catatan</b></th>
<th><b>Tanggal Pengajuan</b></th>
<th><b>Status</b></th>
<th><b>Tanggal Diambil/Pending</b></th>
<th><b>Petugas</b></th>
<th style="display:none;"><b>Kelola</b></th>
            </tr>
        </tfoot>
		</table>
</div>		
                  </div>
              </div>
  </div>
  
               
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery-3.3.1.js"></script>
	<!--<script src="js/dataTables.bootstrap.js"></script>-->
<script src="js/jquery.dataTables2.min.js"></script>
    <script src="js/custom.js"></script>
	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable();
				$('#example2').dataTable();
            });
        </script>
		    
                  </div>
              </div>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
	<!--<script src="dynamitable.jquery.min.js"></script>-->
<script type="text/javascript">

  $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#example').DataTable({
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        columnDefs: [
            { width: '20%', targets: 0 }
        ],
        fixedColumns: true
    });
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );

</script>
	<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
  </body>
</html>

