  
 <!DOCTYPE html>
<?php
// memulai session
session_start();
error_reporting(0);
if (isset($_SESSION['level']))
{
    // jika level admin
    if ($_SESSION['level'] == "admin")
   {   
   }
   // jika kondisi level user maka akan diarahkan ke halaman lain
   else if ($_SESSION['level'] == "1")
   {
       header('location:index.php');
   }
}
if (!isset($_SESSION['level']))
{
    header('location:login.php');
}


$username= $_SESSION['username'];
 ?>
<html>
  <head>
    <title>Antrian Terkini</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/dataTables.bootstrap.css"> 
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <!-- Logo -->
                  <div class="logo">
                     <h1><a href="antrian.php">Antrian Sekolah Pascasarjana</a></h1>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                    <div class="col-lg-12">
                      
                    </div>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="navbar navbar-inverse" role="banner">
                      <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                             <a href="logout.php" >LogOut (<?php echo $_SESSION['username'];?>)</a>
                            
                          </li>
                        </ul>
                      </nav>
                  </div>
               </div>
            </div>
         </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                      <li><a href="kelola_user.php"> Kelola User</a> </li>
                        <li><a href="kelola_mahasiswa.php"> Kelola Mahasiswa</a> </li>
                        <li><a href="kelola_pengajuan.php"> Kelola Pengajuan</a></li>
                        <li><a href="cari2.php"> Antrian sedang Proses</a></li>
                        <li><a href="histori.php">Antrian</a></li>
                        <li><a href="info.php">Informasi</a></li>
                </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="row"></div>

            <div class="row">
                <div class="col-md-12 panel-warning">
                    <div class="content-box-header panel-heading">
                      <div class="panel-options">
                        </div>
                  </div>
                    <div class="content-box-large box-with-header">
                    
<td align="center" colspan="2" >
   
      </td>

<?php 
$link = mysqli_connect("localhost", "root", "", "antrian");
 
// jalankan query
$result = mysqli_query($link, "SELECT COUNT(id_his) as id FROM histori");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{

 
?>  

<tr>
<div class="panel-heading">
<div align="center" class="panel-title">Daftar Antrian</div>
<div class="panel-body">
<table id="example1" class="table table-striped table-bordered">
<thead> 
  <tr>Jumlah Data Antrian = <?php echo $buff['id'];} ?> </tr>
<tr bgcolor="#CCCCCC" align="center">
<td width="20">Nomor Antrian</td>
<td width="20">NRP</td>
<td width="50">Nama Mahasiswa</td>
<td width="50">Jenjang</td>
<td width="50">Mayor</td>
<td width="50">Tujuan</td>
<td width="50">Tanggal Antri</td>
</tr>
</thead>
			<tbody>
<?php
$result = mysqli_query($link, "SELECT * FROM histori,mahasiswa where histori.nrp=mahasiswa.nrp");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{
 
?> 
<tr>
<td width="20" align="center"><?php echo $buff['id_his']; ?></td>
<td width="50"><?php echo $buff['nrp']; ?></td>
<td width="50"><?php echo $buff['nama']; ?></td>
<td width="50"><?php echo $buff['mayor']; ?></td>
<td width="50"><?php echo $buff['prodi']; ?></td>
<td width="50"><?php echo $buff['jenis']; ?></td>
<td width="50"><?php echo $buff['tanggal_pengajuan']; ?></td>
</tr>
<?php
};

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
            </tbody>
		</table>         
                  </div>
              </div>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/dataTables.bootstrap.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
    <script src="js/custom.js"></script>
	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable();
            });
        </script>
  </body>
</html>
