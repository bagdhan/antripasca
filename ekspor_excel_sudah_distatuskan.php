<!DOCTYPE html>
<html>
<head>
	<title>Ekspor Data</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data Antrian yang Sudah Distatuskan.xls");
	?>

	<center>
		<h1>Antrian yang Sudah Distatuskan</h1>
	</center>

	<table border="1">
		<tr>
			<th><b>No.</b></th>
<th><b>No. Registrasi</b></th>
<th><b>NRP</b></th>
<th><b>Nama Mahasiswa</b></th>
<th><b>Nama Surat</b></th>
<th><b>Tanggal Pengajuan</b></th>
<th><b>Estimasi Selesai</b></th>
<th><b>Keterangan</b></th>
<th><b>Catatan</b></th>
<th><b>Status</b></th>
<th><b>Tanggal Proses</b></th>
<th><b>Petugas</b></th>
		</tr>
	<?php
$link2 = mysqli_connect("localhost", "root", "", "antrian");
 $no=1;
$result = mysqli_query($link2, "SELECT * FROM proses,mahasiswa,pengajuan,user where proses.nrp=mahasiswa.nrp and proses.id_pengajuan=pengajuan.id_pengajuan and proses.id_user=user.id_user and status not like 'Proses' ORDER BY id_proses");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{  
 
?> 
		<tr>
			<td><?php echo $no++; ?></td>
<td><?php echo $buff['id_proses']; ?></td>
<td><?php echo $buff['nrp']; ?></td>
<td><?php echo $buff['nama']; ?></td>
<td><?php echo $buff['nama_surat']; ?></td>
<td><?php echo $buff['tanggal_pengajuan']; ?></td>
<td><?php echo $buff['estimasi']; ?></td>
<td><?php echo $buff['keterangan']; ?></td>
<td><?php echo $buff['catatan']; ?></td>
<td><?php echo $buff['status']; ?></td>
<td><?php echo $buff['tanggal_diproses']; ?></td>
<td><?php echo $buff['nama_user']; ?></td>
		</tr>
		<?php
 };

((is_null($___mysqli_res = mysqli_close($link2))) ? false : $___mysqli_res);
?> 
	</table>
</body>
</html>