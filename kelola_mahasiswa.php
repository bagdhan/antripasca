 <!DOCTYPE html>
<?php
// memulai session
session_start();
error_reporting(0);
if (isset($_SESSION['level']))
{
    // jika level admin
    if ($_SESSION['level'] == "admin")
   {   
   }
   // jika kondisi level user maka akan diarahkan ke halaman lain
   else if ($_SESSION['level'] == "1")
   {
       header('location:index.php');
   }
}
if (!isset($_SESSION['level']))
{
    header('location:login.php');
}

$username= $_SESSION['username'];
 ?><html>
  <head>
  <style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
    <title>Antrian Sekolah Pascasarjana</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
        
<link rel="stylesheet" href="css/dataTables.bootstrap.css"> 
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <!-- Logo -->
                  <div class="logo">
                     <h1><a href="admin.php">Antrian Sekolah Pascasarjana</a></h1>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                    <div class="col-lg-12">
                      
                    </div>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="navbar navbar-inverse" role="banner">
                      <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                             <a href="logout.php" >LogOut (<?php echo $_SESSION['username'];?>)</a>
                            
                          </li>
                        </ul>
                      </nav>
                  </div>
               </div>
            </div>
         </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><a href="kelola_user.php"> Kelola Data User</a> </li>
                        <li><a href="kelola_mahasiswa.php"> Kelola Data Mahasiswa</a> </li>
                        <li><a href="kelola_pengajuan.php"> Kelola Data Pengajuan</a></li>
                        <li><a href="cari2.php"> Antrian Sudah Diproses</a></li>
						<li><a href="cari2_2.php"> Antrian yang Sudah Distatuskan</a></li>
                        <!--<li><a href="histori.php">Antrian</a></li>
                        <li><a href="info.php">Informasi</a></li>-->
                </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="row"></div>

            <div class="row">
                <div class="col-md-12 panel-warning">
                    <div class="content-box-header panel-heading">
                      <div class="panel-options">
                        </div>
                  </div>
                    <div class="content-box-large box-with-header">
                    


<?php 
 
$link = mysqli_connect("localhost", "root", "", "antrian");
 
?>  <tr>
<div class="panel-heading">
<div align="center" class="panel-title">Kelola Data Mahasiswa</div>
<div class="panel-body">
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Aktif')">Mahasiswa Aktif</button>
  <button class="tablinks" onclick="openCity(event, 'Non_aktif')">Mahasiswa Non Aktif</button>
</div>
<div id="Aktif" class="tabcontent">
<a href="mahasiswa_aktif.php" class="btn btn-success">Tambah Data Mahasiswa Aktif</a><br><br><br>
<table id="example1" class="table table-striped table-bordered">
<thead> 
<tr>
<td width="50"><b>No.</b></td>
<td width="50"><b>NRP</b></td>
<td width="100"><b>Nama Lengkap</b></td>
<td width="20"><b>Mayor</b></td>
<td width="20"><b>Program Studi</b></td>
<td width="20"><b>Edit</b></td>
<td width="20"><b>Hapus</b></td>
</tr>
</thead>
			<tbody>
<?php
 $no=1;
 $result = mysqli_query($link, "SELECT * FROM mahasiswa_aktif");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{
 
?> 
<tr>
<td><?php echo $no; ?></td>
<td><?php echo $buff['nrp']; ?></td>
<td><?php echo $buff['nama']; ?></td>
<td><?php echo $buff['mayor']; ?></td>
<td><?php echo $buff['prodi']; ?></td>

<td align="center"><a href="edit_mahasiswa_aktif.php?nrp=<?php echo $buff['nrp']; ?>"><img src='edit.png' style='width:30px; height:27px;'></a></td>
<td><a href="del3_aktif.php?nrp=<?php echo $buff['nrp']; ?>"> <img src='del.png' style='width:30px; height:27px;'> </a></td>
</tr>

<?php
$no++; };

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
   </tbody>
		</table>
</div>
<div id="Non_aktif" class="tabcontent">
<a href="mahasiswa_tidakaktif.php" class="btn btn-success">Tambah Data Mahasiswa Tidak Aktif</a><br><br><br>
<table id="example2" class="table table-striped table-bordered">
<thead> 
<tr>
<td width="50"><b>No.</b></td>
<td width="50"><b>NRP</b></td>
<td width="100"><b>Nama Lengkap</b></td>
<td width="20"><b>Mayor</b></td>
<td width="20"><b>Program Studi</b></td>
<td width="20"><b>Edit</b></td>
<td width="20"><b>Hapus</b></td>
</tr>
</thead>
			<tbody>
<?php
$link2 = mysqli_connect("localhost", "root", "", "antrian");
 $no=1;
 $result = mysqli_query($link2, "SELECT * FROM mahasiswa_tidakaktif");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{
 
?> 
<tr>
<td><?php echo $no; ?></td>
<td><?php echo $buff['nrp']; ?></td>
<td><?php echo $buff['nama']; ?></td>
<td><?php echo $buff['mayor']; ?></td>
<td><?php echo $buff['prodi']; ?></td>

<td align="center"><a href="edit_mahasiswa_tidakaktif.php?nrp=<?php echo $buff['nrp']; ?>"><img src='edit.png' style='width:30px; height:27px;'></a></td>
<td><a href="del3_tidakaktif.php?nrp=<?php echo $buff['nrp']; ?>"> <img src='del.png' style='width:30px; height:27px;'> </a></td>
</tr>

<?php
$no++; };

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
   </tbody>
		</table>
</div>		
                  </div>
              </div>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
    <script src="https://code.jquery.com/jquery.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/dataTables.bootstrap.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable();
				$('#example2').dataTable();
            });
        </script>
  </body>
</html>

