<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sekolah Pascasarjana IPB</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		    <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <style>
            @import url('https://fonts.googleapis.com/css?family=Titillium+Web:400,700');
            @font-face {
                font-family: 'Titillium Web';
                src: url(Titillium Web.ttf);
            }
            table{
                border-collapse: collapse;
                width: 80%;
                height: auto;
            }
            td {border: 1px solid;}
        </style>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

      <div class="container-fluid">
        <div class="row">

          <!-- KOLOM KIRI -->
          <div class="col-md-7">

            <!-- AWAL BAGIAN LOGO -->
            <div class="row">

                <div class="col-md-2">
                  <div class="bungkus-kepala">
                    <div class="logo"></div>
                  </div>
                </div>
                <div class="col-md-10">
                  <div class="bungkus-kepala">
                  <div id="nama" class="nama">
                    <span class="n1">Sekolah Pascasarjana</span><br><span class="n2">Institut Pertanian Bogor</span>
                  </div>
                </div>
              </div>
            </div>
            <!-- AKHIR BAGIAN LOGO -->

            <!-- AWAL BAGIAN KOLOM ANTRIAN -->
            <div class="row">
              <div class="col-md-12">                

                <!-- AWAL BAGIAN ANTRIAN -->
                <div class="form-box-kiri">
                  <div class="form-top">
                    <div class="form-top-left">
                      <h3><b>ANTRIAN LOKET</b></h3>
                    </div>
                    <div class="form-top-right">
                      <i class="fa fa-microphone"></i>
                    </div>
                  </div>
                    <div class="form-content">                              

            <table>
                
                                <tr class='loket'><td>LOKET &ensp;1</td></tr>
                                <td><div id='wrap-antrian'><div class='box-antrian'><span>ANTRIAN</span></div></div></td>
                                <td class='nomerantri'><strong></strong></td>     <!--ganti variabel disini dengan memanggil id antrian-->
                                <tr><td class='pemisah'>&nbsp;</td></tr>
								<tr class='loket'><td>LOKET &ensp;2</td></tr>
                                <td><div id='wrap-antrian'><div class='box-antrian'><span>ANTRIAN</span></div></div></td>
                                <td class='nomerantri'><a></a></td>     <!--ganti variabel disini dengan memanggil id antrian-->
                                <tr><td class='pemisah'>&nbsp;</td></tr>
								<tr class='loket'><td>LOKET &ensp;3</td></tr>
                                <td><div id='wrap-antrian'><div class='box-antrian'><span>ANTRIAN</span></div></div></td>
                                <td class='nomerantri'><font></font></td>     <!--ganti variabel disini dengan memanggil id antrian-->
                                <tr><td class='pemisah'>&nbsp;</td></tr>
								<tr class='loket'><td>LOKET &ensp;4</td></tr>
                                <td><div id='wrap-antrian'><div class='box-antrian'><span>ANTRIAN</span></div></div></td>
                                <td class='nomerantri'><p></p></td>     <!--ganti variabel disini dengan memanggil id antrian-->
                                <tr><td class='pemisah'>&nbsp;</td></tr>
                            <tr></tr>
            </table>
            <script type="text/javascript" src="jquery.js"></script>
            <script type="text/javascript" src="tampil.js"></script>
			<script type="text/javascript" src="tampil2.js"></script>
			<script type="text/javascript" src="tampil3.js"></script>
			<script type="text/javascript" src="tampil4.js"></script>
<!-- border-top-color: #fff; -->

                    </div>
                 </div>
                 <!-- AKHIR BAGIAN ANTRIAN -->
                 

              </div>
            </div>
            <!-- AKHIR BAGIAN KOLOM ANTRIAN -->

          </div>
          <!-- AKHIR KOLOM KIRI -->


          <!-- AWAL KOLOM KANAN -->
          <div class="col-md-5">

            <!-- AWAL BAGIAN INFORMASI -->
            <div class="form-box-kanan">
              <div class="form-top">
                <div class="form-top-left">
                    <h3><b>SISTEM INFORMASI DIGITAL SPs IPB</b></h3>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-info"></i>
                </div>
              </div>
              <div class="form-info">
				  <iframe width="830" height="640" src="https://www.youtube.com/embed/videoseries?list=PLnB0Rqm_wcnNKJR23bD3aQglCg-szw6kJ&autoplay=1&loop=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				  <!--<audio width="830" height="640" controls loop autoplay hidden>
				  <source src="lagu.ogg" type="audio/ogg">
				  <source src="lagu.mp3" type="audio/mpeg">
				  </audio>-->
                  <!--<?php $link2 = mysqli_connect("localhost", "root", "", "antrian");
                  // jalankan query
                  $result2 = mysqli_query($link2, "SELECT * FROM informasi where id_info='1'");
                  // tampilkan query
                  while ($row2=mysqli_fetch_array($result2,MYSQLI_ASSOC))
                  { ?>
                      <marquee scrolldelay="200" direction="up"><p><?php echo $row2['info']; ?></p></marquee>
                  <?php } ?>!-->
              </div>  <!-- form-info -->
              <div class="space"></div>
              <div class="form-jam">

                <div class="row">
                  <div class="col-md-4">
                    <div class="hari">
                      <?php
                        $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jum`at", "Sabtu","Minggu");
                        $hari = $array_hari[date("N")];
                      ?>
                      <?=$hari;?>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="jam">
                      <script type="text/javascript">
                          function waktu() {
                            var time = new Date(),
                            jam = time.getHours(),
                            menit = time.getMinutes(),
                            detik = time.getSeconds();
                            document.querySelectorAll('.jam')[0].innerHTML = harold(jam) + " : " + harold(menit) + " : " + harold(detik);

                          function harold(standIn) {
                            if (standIn < 10) {
                                standIn = '0' + standIn
                            }
                            return standIn;
                            }
                          }
                          setInterval(waktu, 1000);
                      </script>
                    </div>
                  </div>
                </div>

              </div>  <!-- form-jam -->
            </div>  <!-- form-box-kanan -->
            <!-- AKHIR BAGIAN INFORMASI -->

          </div>  <!-- col-md-5 -->
          <!-- AKHIR KOLOM KANAN -->

        </div>
      </div>

        <!-- Footer -->
        <!--<footer>
        	<div class="container">
        		<div class="row">

        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				<p>Made by Anli Zaimi at <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a>
        					having a lot of fun. <i class="fa fa-smile-o"></i></p>
        			</div>


              </div>

        		</div>
        	</div>
        </footer>!-->
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>
