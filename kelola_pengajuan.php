 <!DOCTYPE html>
<?php
// memulai session
session_start();
error_reporting(0);
if (isset($_SESSION['level']))
{
    // jika level admin
    if ($_SESSION['level'] == "admin")
   {   
   }
   // jika kondisi level user maka akan diarahkan ke halaman lain
   else if ($_SESSION['level'] == "1")
   {
       header('location:index.php');
   }
}
if (!isset($_SESSION['level']))
{
    header('location:login.php');
}

$username= $_SESSION['username'];
 ?><html>
  <head>
    <title>Antrian Sekolah Pascasarjana</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
        
<link rel="stylesheet" href="css/dataTables.bootstrap.css"> 
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <!-- Logo -->
                  <div class="logo">
                     <h1><a href="admin.php">Antrian Sekolah Pascasarjana</a></h1>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                    <div class="col-lg-12">
                      
                    </div>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="navbar navbar-inverse" role="banner">
                      <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                             <a href="logout.php" >LogOut (<?php echo $_SESSION['username'];?>)</a>
                            
                          </li>
                        </ul>
                      </nav>
                  </div>
               </div>
            </div>
         </div>
    </div>

    <div class="page-content">
        <div class="row">
          <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><a href="kelola_user.php"> Kelola Data User</a> </li>
                        <li><a href="kelola_mahasiswa.php"> Kelola Data Mahasiswa</a> </li>
                        <li><a href="kelola_pengajuan.php"> Kelola Data Pengajuan</a></li>
                        <li><a href="cari2.php"> Antrian Sudah Diproses</a></li>
						<li><a href="cari2_2.php"> Antrian yang Sudah Distatuskan</a></li>
                        <!--<li><a href="histori.php">Antrian</a></li>
                        <li><a href="info.php">Informasi</a></li>-->
                </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="row"></div>

            <div class="row">
                <div class="col-md-12 panel-warning">
                    <div class="content-box-header panel-heading">
                      <div class="panel-options">
                        </div>
                  </div>
                    <div class="content-box-large box-with-header">
                    


<?php 
 
$link = mysqli_connect("localhost", "root", "", "antrian");
 
?>  <tr>
<div class="panel-heading">
<a href="pengajuan.php" class="btn btn-success">Tambah Data Pengajuan</a>
<div align="center" class="panel-title">Jenis Pengajuan</div>
<div class="panel-body">
<table id="example1" class="table table-striped table-bordered">
<thead> 
<tr>
<td width="50"><b>No.</b></td>
<td width="50"><b>Nama Pengajuan</b></td>
<td width="100"><b>Persyaratan</b></td>
<td width="20"><b>Waktu Penyelesaian</b></td>
<td width="20"><b>Edit</b></td>
<td width="20"><b>Hapus</b></td>
</tr>
</thead>
			<tbody>
<?php
$result = mysqli_query($link, "SELECT * FROM pengajuan");
 
// tampilkan query
while ($buff=mysqli_fetch_array($result,MYSQLI_ASSOC))
{ 
 
?> 
<tr>
<td><?php echo $buff['id_pengajuan']; ?></td>
<td><?php echo $buff['nama_surat']; ?></td>
<td><?php echo $buff['syarat']; ?></td>
<td><?php echo $buff['waktu']; ?></td>

<td align="center"><a href="edit_pengajuan.php?nama_surat=<?php echo $buff['nama_surat']; ?>"><img src='edit.png' style='width:30px; height:27px;'></a></td>
<td><a href="del2.php?nama_surat=<?php echo $buff['nama_surat']; ?>"> <img src='del.png' style='width:30px; height:27px;'> </a></td>
</tr>

<?php
};

((is_null($___mysqli_res = mysqli_close($link))) ? false : $___mysqli_res);
?> 
   </tbody>
		</table>                 
                  </div>
              </div>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
    <script src="https://code.jquery.com/jquery.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/dataTables.bootstrap.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
    <script src="js/custom.js"></script>
	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable();
            });
        </script>
  </body>
</html>

